#version 400
layout(location = 0) in float coord1d;
out vec4 frag_colour;

uniform float scale;
uniform float offset;
uniform sampler2D mytexture;

void main(void)
{
	float x = (coord1d / scale) - offset;
	float y = (texture2D(mytexture, vec2(x / 10.24 / 2.0 + 0.5, 0)).r - 0.5) * 2.0;
	
	gl_Position = vec4( coord1d, y, 0, 1);
	frag_colour = vec4( x/2.0 + 0.5, y/2.0 + 0.5 , 1 ,  1 );
}