#version 400
layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 colour;
out vec3 frag_colour;

// Values that stay constant for the whole mesh.
uniform mat4 MVP;

void main()
{
	frag_colour = colour;
	gl_Position = MVP*vec4(pos, 1.0);
}