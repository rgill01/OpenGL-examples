#include <util.h>
#include <stdio.h>
#include <assert.h>
#include <vectorf.h>
#include <utilities.h>

#if 0
#include <glm/gtc/matrix_transform.hpp> 
#include <glm/gtx/transform.hpp>
#endif

// An array of 3 vectors which represents 3 vertices
static const GLfloat g_vertex_buffer_data[] = 
{
  -1.0f, -1.0f, 0.0f,
   1.0f, -1.0f, 0.0f,
   0.0f,  1.0f, 0.0f,
};


int main()
{
	GLFWwindow* window = util_start("ex3-triangle coloured");
	assert(window != NULL);

	/* create vertex buffer object */

	// This will identify our vertex buffer
	GLuint vertexbuffer;
	// Generate 1 buffer, put the resulting identifier in vertexbuffer
	glGenBuffers(1, &vertexbuffer);
	// The following commands will talk about our 'vertexbuffer' buffer
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	// Give our vertices to OpenGL.
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);

	/* create vertex array  */

	GLuint vertexArrayObject;
	glGenVertexArrays(1, &vertexArrayObject);
	glBindVertexArray(vertexArrayObject);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);

	/* shader program */

	GLuint shader = util_loadshaders("vertex_shader.glsl", "fragment_shader.glsl");
	
	/* transform */
#if 1
	rotf q; 
	rotf_from_yaw(radiansf(90), &q);

	mat4f proj; 
  mat4f_perspective(radiansf(90), 4/3.0, 0.1, 100, &proj);
  
  mat4f view;
  vec3f cPos = {0,10,3}, 
  			cTar = {0,0,0}, 
  			cUp  = {0,1,0};
  mat4f_look_at(&cPos, &cTar, &cUp, &view);

  mat4f_print(&view);

  mat4f T;
  mat4f_rot_trans_camera(&q, &vec3f_zero, &view, &proj, &T);
  // mat4f_copy(&mat4f_identity, &T);
  mat4f_print(&T);
#else
	// Projection matrix : 45° Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
glm::mat4 Projection = glm::perspective(glm::radians(45.0f), (float) 4.f/3.f, 0.1f, 100.0f);
  
// Or, for an ortho camera :
//glm::mat4 Projection = glm::ortho(-10.0f,10.0f,-10.0f,10.0f,0.0f,100.0f); // In world coordinates
  
// Camera matrix
glm::mat4 View = glm::lookAt(
    glm::vec3(4,3,3), // Camera is at (4,3,3), in World Space
    glm::vec3(0,0,0), // and looks at the origin
    glm::vec3(0,1,0)  // Head is up (set to 0,-1,0 to look upside-down)
    );
  mat4f_print((mat4f *) &View);
// Model matrix : an identity matrix (model will be at the origin)
glm::mat4 Model = glm::mat4(1.0f);
// Our ModelViewProjection : multiplication of our 3 matrices
glm::mat4 T = Projection * View * Model; // Remember, matrix multiplication is the other way around

	mat4f_print((mat4f *) &T);
#endif

  GLuint matrixID = glGetUniformLocation(shader, "MVP");
  glUseProgram(shader);
  glUniformMatrix4fv(matrixID, 1, GL_TRUE, (GLfloat *) &T[0][0]);


	glClearColor(0.3, 0.3, 0.3, 1.0);
  
  // Ensure we can capture the escape key being pressed below
  glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	
	while(!glfwWindowShouldClose(window) && 
    glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS) 
	{
	  // wipe the drawing surface clear
	  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	 	
	 	//
		// Note: this call is not necessary, but I like to do it anyway before any
		// time that I call glDrawArrays() so I never use the wrong shader programme
		// glUseProgram( shader );

		// Note: this call is not necessary, but I like to do it anyway before any
		// time that I call glDrawArrays() so I never use the wrong vertex data
	  // glBindVertexArray(vertexArrayObject);
	  
	  // draw points 0-3 from the currently bound VAO with current in-use shader
	  glDrawArrays(GL_TRIANGLES, 0, 3);

	  // update other events like input handling 
	  glfwPollEvents();
	  // put the stuff we've been drawing onto the display
	  glfwSwapBuffers(window);
	}

	



	glDeleteBuffers(1, &vertexbuffer);
	glDeleteVertexArrays(1, &vertexArrayObject);
	glDeleteProgram(shader);
	glfwTerminate();
	return 0;
}