#version 400
layout(location = 0) in vec2 pt;
out vec4 frag_colour;

uniform float scale;
uniform float offset;

void main(void)
{
	gl_Position = vec4( (pt.x + offset)*scale, pt.y, 0, 1);
	frag_colour = vec4( pt.xy/2.0 + 0.5, 1, 1);
}