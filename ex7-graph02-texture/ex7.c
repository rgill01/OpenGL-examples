#include <util.h>
#include <stdio.h>
#include <assert.h>
#include <vectorf.h>
#include <utilities.h>



int main()
{
	int vertex_texture_units;
	glGetIntegerv(GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS, &vertex_texture_units);

	if(!vertex_texture_units) {
	  fprintf(stderr, "Your graphics cards does not support texture lookups in the vertex shader!\n");
	  // exit here or use another method to render the graph
	  return -1;
	}

	GLFWwindow* window = util_start("ex6-graph01");
	assert(window != NULL);

	GLfloat line[101];
	for(int i = 0; i < 101; i++) {
	  line[i] = (i-50.f)/50.f;
	}

	/* create vertex buffer object */

	GLuint vbo;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(line), 
		line, GL_STATIC_DRAW);

	/* create vertex array  */

	GLuint vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glVertexAttribPointer(0, 1, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);


	/* same for graph */

	GLbyte graph[2048];
	for(int i = 0; i < 2048; i++) {
	  float x = (i - 1024.0) / 100.0;
	  float y = sinf(x * 10.0) / (1.0 + x * x);
	  graph[i] = roundf(y * 128 + 128);
	}
	GLuint texture_id;
	glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &texture_id);
	glBindTexture(GL_TEXTURE_2D, texture_id);
	glTexImage2D(
	  GL_TEXTURE_2D,      // target
	  0,                  // level, 0 = base, no minimap,
	  GL_LUMINANCE,       // internalformat
	  2048,               // width
	  1,                  // height
	  0,                  // border, always 0 in OpenGL ES
	  GL_LUMINANCE,       // format
	  GL_UNSIGNED_BYTE,   // type
	  graph
	);

	// glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	// glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	/* shader program */

	GLuint shader = util_loadshaders("vertex_shader.glsl", "fragment_shader.glsl");
	glUseProgram(shader);

	GLuint scaleID = glGetUniformLocation(shader, "scale");
	GLuint offsetID = glGetUniformLocation(shader, "offset");

	// glUniform1f(scaleID,  0.1);
	// glUniform1f(offsetID, 0);
	float scale = 1/(10.24), offset = 0;
	GLenum mode = 0;
	/* begin main */

	glClearColor(0.3, 0.3, 0.3, 1.0);


	// Cull triangles which normal is not towards the camera
	// glEnable(GL_CULL_FACE);

	while(!glfwWindowShouldClose(window) && 
	glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS) 
	{

		// wipe the drawing surface clear
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);



		// play with inputs

		if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS ) { 
			scale *= 2;
		}
		if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) {
			scale *= 0.5;
		}
		if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
			offset += 0.1;
		}
		if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
			offset -= 0.1;
		}
		
		if (glfwGetKey(window, GLFW_KEY_F1) == GLFW_PRESS) {
			mode = GL_POINTS;
		}
		else if (glfwGetKey(window, GLFW_KEY_F2) == GLFW_PRESS) {
			mode = GL_LINE;
		}
		else if (glfwGetKey(window, GLFW_KEY_F3) == GLFW_PRESS) {
			mode = GL_LINE_STRIP;
		}
		else if (glfwGetKey(window, GLFW_KEY_F4) == GLFW_PRESS) {
			mode = GL_TRIANGLES;
		}

		glUniform1f(scaleID, scale);
		glUniform1f(offsetID, offset);




		// draw these many points
		glDrawArrays(mode, 0, 2000);

		// update other events like input handling 
		glfwPollEvents();
		// put the stuff we've been drawing onto the display
		glfwSwapBuffers(window);
	}

	glDeleteBuffers(1, &vbo);
	glDeleteVertexArrays(1, &vao);
	glDeleteProgram(shader);
	glfwTerminate();
	return 0;
}