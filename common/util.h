#ifndef __COMMON_UTIL_H_
#define __COMMON_UTIL_H_

#ifdef USE_GLEW /* hack so I can use easyclang */
    #include <GL/glew.h>
#else
    #warning "Not using GLEW"
    #define GL_GLEXT_PROTOTYPES
    #include <GL/gl.h>
    #include <GL/glext.h>
#endif
#define GLFW_DLL			
#include <GLFW/glfw3.h> /* for creating an OpenGL context. Does this get replaced by GTK? */
#include <vectorf.h>

/*****************************************************************************
 * Public API 
 ****************************************************************************/

/* shaders related */

/** util_loadshaders
  * returns: program id 
  */
GLuint util_loadshaders(const char * vertex_file_path, 
												const char * fragment_file_path);


/** util_start
  * perform boiler plate code, return window pointer 
  */
GLFWwindow* util_start(const char *title);

/** util_compute_camera_from_input
  * get camera transformations based on user input 
  */
void util_compute_camera_from_input(GLFWwindow *window, mat4f *view, mat4f *proj);

void util_compute_rotate_camera_from_input(GLFWwindow *window, mat4f *view, mat4f *proj);


#endif /* include guard */