#version 400
layout(location = 0) in vec3 pos;

// Values that stay constant for the whole mesh.
uniform mat4 MVP;

void main()
{
	gl_Position = MVP*vec4(pos, 1.0);
}