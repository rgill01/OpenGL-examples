#version 400
in vec3 frag_colour;
out vec4 colour;

uniform vec4 rgba;

void main()
{
	colour = rgba;
}