#ifdef USE_GLEW /* hack so I can use easyclang */
    #include <GL/glew.h>
#else
    #warning "Not using GLEW"
    #define GL_GLEXT_PROTOTYPES
    #include <GL/gl.h>
    #include <GL/glext.h>
#endif
#define GLFW_DLL			
#include <GLFW/glfw3.h> /* for creating an OpenGL context. Does this get replaced by GTK? */

#include <stdio.h>

int main()
{
  // start GL context and O/S window using the GLFW helper library
  if (!glfwInit()) {
    fprintf(stderr, "ERROR: could not start GLFW3\n");
    return 1;
  } 

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  GLFWwindow* window = glfwCreateWindow(640, 480, "Hello Triangle", NULL, NULL);
  if (!window) {
    fprintf(stderr, "ERROR: could not open window with GLFW3\n");
    glfwTerminate();
    return 1;
  }
  glfwMakeContextCurrent(window);
                                  
  // start GLEW extension handler
#ifdef USE_GLEW
  glewExperimental = GL_TRUE;
  if (glewInit())
  {
      fprintf(stderr, "Failed to initialize GLEW.\n");
  }
#endif

  // get version info
  const GLubyte* renderer = glGetString(GL_RENDERER); // get renderer string
  const GLubyte* version = glGetString(GL_VERSION); // version as a string
  printf("Renderer: %s\n", renderer);
  printf("OpenGL version supported %s\n", version);

  // tell GL to only draw onto a pixel if the shape is closer to the viewer
  glEnable(GL_DEPTH_TEST); // enable depth-testing
  glDepthFunc(GL_LESS); // depth-testing interprets a smaller value as "closer"

  /* OTHER STUFF GOES HERE NEXT */

  float points[] = { -0.5,  0.5,  0.0,
  									  0.5,  0.5,  0.0,
  									  0.5, -0.5,  0.0};

  float colours[] = { 1, 0, 0,
                      0, 1, 0,
                      0, 0, 1,};

  GLuint pvbo = 0; /* vertex buffer object that will be on the graphics card */
  glGenBuffers(1, &pvbo);
  glBindBuffer(GL_ARRAY_BUFFER, pvbo); /* set to current buffer in OpenGL's SM */
  glBufferData(GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW); /* copy to buffer */

  GLuint cvbo = 0;
  glGenBuffers(1, &cvbo);
  glBindBuffer(GL_ARRAY_BUFFER, cvbo); /* set to current buffer in OpenGL's SM */
  glBufferData(GL_ARRAY_BUFFER, sizeof(colours), colours, GL_STATIC_DRAW);

  /* Our object now consists of 2 vertex buffers, which will be input "attribute" 
  variables to our vertex shader. We set up the layout of both of these with a 
  single vertex array object - the VAO represents our complete object, so we 
  no longer need to keep track of the individual VBOs.  \Note that we have to 
  bind each VBO before calling glVertexAttribPointer(), which describes the 
  layout of each buffer to the VAO.
   */
  
  GLuint vao = 0;
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);
  
  glBindBuffer(GL_ARRAY_BUFFER, pvbo);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
  glBindBuffer(GL_ARRAY_BUFFER, cvbo);
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);

  glEnableVertexAttribArray(0);  //points
  glEnableVertexAttribArray(1);  //colours


  /* This shader programme is made from the minimum 2 parts; a vertex shader, 
  which describes where the 3d points should end up on the display, and a 
  fragment shader which colours the surfaces. Both are be written in plain 
  text, and look a lot like C programmes. Loading these from plain-text files 
  would be nicer 
  */

  const char* vertex_shader =
		"#version 400\n"
    "layout(location = 0) in vec3 vertex_position;"
    "layout(location = 1) in vec3 vertex_colour;"
		"out vec3 colour;"
		"void main() {"
    " colour = vertex_colour;"
		" gl_Position = vec4(vertex_position, 1);"
		"}";

	const char* fragment_shader =
		"#version 400\n"
    "in vec3 colour;"
		"out vec4 frag_colour;"
		"void main() {"
		"  frag_colour = vec4(colour, 1.0);"
		"}";

	GLuint vs = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vs, 1, &vertex_shader, NULL);
	glCompileShader(vs);
	GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fs, 1, &fragment_shader, NULL);
	glCompileShader(fs);

	GLuint shader_programme = glCreateProgram();
	glAttachShader(shader_programme, fs);
	glAttachShader(shader_programme, vs);
	glLinkProgram(shader_programme);

	glClearColor(0.3, 0.3, 0.3, 1.0);
	
	while(!glfwWindowShouldClose(window)) 
	{
	  // wipe the drawing surface clear
	  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	  glUseProgram(shader_programme);
	  glBindVertexArray(vao);
	  // draw points 0-3 from the currently bound VAO with current in-use shader
	  glDrawArrays(GL_TRIANGLES, 0, 3);

	  // update other events like input handling 
	  glfwPollEvents();
	  // put the stuff we've been drawing onto the display
	  glfwSwapBuffers(window);
	}

  // close GL context and any other GLFW resources
  glfwTerminate();
  return 0;
}
