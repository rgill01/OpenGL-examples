#include <util.h>
#include <stdio.h>
#include <assert.h>
#include <vectorf.h>
#include <utilities.h>

#include <freetype2/ft2build.h>
#include FT_FREETYPE_H

#define FONT_TFF 			"/usr/local/texlive/2016/texmf-dist/fonts/truetype/google/roboto/RobotoCondensed-BoldItalic.ttf"
//"/usr/local/texlive/2016/texmf-dist/fonts/truetype/public/comfortaa/Comfortaa-Light.ttf"

static int window_width, window_height;

static void render_text(const FT_Face f, const char *text, float x, float y, float sx, float sy) {
  const char *p;
  if (f == NULL) {
  	printf("face is Null\n");
  	return;
  } 

	FT_GlyphSlot g = f->glyph;

  for(p = text; *p; p++) {
    if(FT_Load_Char(f, *p, FT_LOAD_RENDER))
        continue;
 
    glTexImage2D(
      GL_TEXTURE_2D,
      0,
      GL_RED,
      g->bitmap.width,
      g->bitmap.rows,
      0,
      GL_RED,
      GL_UNSIGNED_BYTE,
      g->bitmap.buffer
    );
 
    float x2 = x + g->bitmap_left * sx;
    float y2 = -y - g->bitmap_top * sy;
    float w = g->bitmap.width * sx;
    float h = g->bitmap.rows * sy;
 
    GLfloat box[4][4] = {
        {x2,     -y2    , 0, 0},
        {x2 + w, -y2    , 0, 1},
        {x2,     -y2 - h, 1, 0},
        {x2 + w, -y2 - h, 1, 1},
    };
 
    glBufferData(GL_ARRAY_BUFFER, sizeof(box), box, GL_DYNAMIC_DRAW);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
 
    x += (g->advance.x/64) * sx;
    y += (g->advance.y/64) * sy;

  }
}




int main()
{
	GLFWwindow* window = util_start("ex9-text");
	assert(window != NULL);

	FT_Library ft;
	if(FT_Init_FreeType(&ft)) {
		fprintf(stderr, "Could not init freetype library\n");
		return 1;
	}

	FT_Face face;
	if(FT_New_Face(ft, FONT_TFF, 0, &face)) {
	  fprintf(stderr, "Could not open font\n");
	  return 1;
	} 
	else if (face == NULL){
		printf("face is NULL\n");
		return 1;
	}

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


	GLuint tex;
	glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);


	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);


	/* shader program */

	GLuint shader = util_loadshaders("vertex_shader.glsl", "fragment_shader.glsl");
	glUseProgram(shader);

	/* vbo */

	GLuint vbo;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	/* vao */

	GLuint vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);
 	

	/* uniforms */

	GLuint uniform_color = glGetUniformLocation(shader, "color");
	// GLuint uniform_tex   = glGetUniformLocation(shader, "tex");

	// glUniform1i(uniform_tex, 0);

	/* main loop */

	glClearColor(1, 1, 1, 1);

	while(!glfwWindowShouldClose(window) && 
	glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS) 
	{	
		glClear(GL_COLOR_BUFFER_BIT);		glViewport(0, 0, window_width, window_height);


	  GLfloat black[4] = {0, 0, 0, 1};
	  glUniform4fv(uniform_color, 1, black);

	  glfwGetWindowSize(window, &window_width, &window_height);
	  float sx = 2.0 / (float) window_width;
	  float sy = 2.0 / window_height; /* to convert pixels to [-1,1] */

		FT_Set_Pixel_Sizes(face, 0, 48);
	  render_text(face, "The Quick Brown Fox Jumps Over The Lazy Dog",
	              -1 + 8 * sx,   1 - 50 * sy,    sx, sy);
	  render_text(face, "The Misaligned Fox Jumps Over The Lazy Dog",
	              -1 + 8.5 * sx, 1 - 100.5 * sy, sx, sy);

    FT_Set_Pixel_Sizes(face, 0, 48);
	  render_text(face, "The Small Texture Scaled Fox Jumps Over The Lazy Dog",
	              -1 + 8 * sx,   1 - 175 * sy,   sx * 0.5, sy * 0.5);

	  FT_Set_Pixel_Sizes(face, 0, 24);
	  render_text(face, "The Small Font Sized Fox Jumps Over The Lazy Dog",
	              -1 + 8 * sx,   1 - 200 * sy,   sx, sy);

	  FT_Set_Pixel_Sizes(face, 0, 48);
	  render_text(face, "The Tiny Texture Scaled Fox Jumps Over The Lazy Dog",
	              -1 + 8 * sx,   1 - 235 * sy,   sx * 0.25, sy * 0.25);

	  FT_Set_Pixel_Sizes(face, 0, 12);
	  render_text(face, "The Tiny Font Sized Fox Jumps Over The Lazy Dog",
	              -1 + 8 * sx,   1 - 250 * sy,   sx, sy);


	  FT_Set_Pixel_Sizes(face, 0, 48);
  	render_text(face, "The Solid Black Fox Jumps Over The Lazy Dog",
              -1 + 8 * sx,   1 - 430 * sy,   sx, sy);

  	GLfloat red[4] = {1, 0, 0, 1};
  	glUniform4fv(uniform_color, 1, red);
	  render_text(face, "The Solid Red Fox Jumps Over The Lazy Dog",
	              -1 + 8 * sx,   1 - 330 * sy,   sx, sy);
	  render_text(face, "The Solid Red Fox Jumps Over The Lazy Dog",
	              -1 + 28 * sx,  1 - 450 * sy,   sx, sy);

	  GLfloat transparent_green[4] = {0, 1, 0, 0.5};
	  glUniform4fv(uniform_color, 1, transparent_green);
	  render_text(face, "The Transparent Green Fox Jumps Over The Lazy Dog",
	              -1 + 8 * sx,   1 - 380 * sy,   sx, sy);
	  render_text(face, "The Transparent Green Fox Jumps Over The Lazy Dog",
	              -1 + 18 * sx,  1 - 440 * sy,   sx, sy);


		// update other events like input handling 
		glfwPollEvents();
		// put the stuff we've been drawing onto the display
		glfwSwapBuffers(window);
	}

	glDeleteBuffers(1, &vbo);
	glDeleteVertexArrays(1, &vao);
	glDeleteProgram(shader);
	glfwTerminate();
	return 0;
}