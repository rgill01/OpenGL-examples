#include <util.h>
#include <stdio.h>
#include <assert.h>
#include <vectorf.h>
#include <utilities.h>
#include <freetype2/ft2build.h>

#include FT_FREETYPE_H
#define FONT_TFF 			"/usr/local/texlive/2016/texmf-dist/fonts/truetype/public/comfortaa/Comfortaa-Light.ttf"
#define PADDING 				0
#define MAX_TEXT_CHARS  200

static const int margin 	= 20;
static const int ticksize = 10;
int window_width, window_height;

static GLuint textshader,texID, texBuffer, texUniformColour, texArrayObj;
static int tex_n;

static int atlas_width, atlas_height;
static struct character_info {
  float ax; // advance.x
  float ay; // advance.y
  
  float bw; // bitmap.width;
  float bh; // bitmap.rows;
  
  float bl; // bitmap_left;
  float bt; // bitmap_top;
  
  float tx; // x offset of glyph in texture coordinates
} c[128] = {{0}};

static void create_atlas(void)
{
	FT_Library ft;
	if(FT_Init_FreeType(&ft)) {
		fprintf(stderr, "Could not init freetype library\n");
		return;
	}

	FT_Face face;
	if(FT_New_Face(ft, FONT_TFF, 0, &face)) {
	  fprintf(stderr, "Could not open font\n");
	  return;
	} 
	else if (face == NULL){
		printf("face is NULL\n");
		return;
	}
	FT_Set_Pixel_Sizes(face, 0, 12);	


	FT_GlyphSlot g = face->glyph;
	unsigned int w = 0;
	unsigned int h = 0;

	for(int i = 32; i < 128; i++) {
	  if(FT_Load_Char(face, i, FT_LOAD_RENDER)) {
	    fprintf(stderr, "Loading character %d:%c failed!\n", i,i);
	    continue;
  	}
  	w += g->bitmap.width + PADDING;
  	h = maxui(h, g->bitmap.rows);
	}

	/* you might as well save this value as it is needed later on */
	atlas_width = w;
	atlas_height = h;

	glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &texID);
	glBindTexture(GL_TEXTURE_2D, texID);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);


	glTexImage2D(
		GL_TEXTURE_2D, 
		0, 
		GL_RED, 
		w, 
		h, 
		0, 
		GL_RED, 
		GL_UNSIGNED_BYTE, 
		0);


	int x = 0;
	for(int i = 32; i < 128; i++) {
  	if(FT_Load_Char(face, i, FT_LOAD_RENDER)){
  		printf("error loading char %d\n", i);
    	continue;
  	}

  	glTexSubImage2D(
  		GL_TEXTURE_2D, 
  		0, 
  		x, 
  		0, 
  		g->bitmap.width+PADDING, 
  		g->bitmap.rows, 
  		GL_RED, 
  		GL_UNSIGNED_BYTE, 
  		g->bitmap.buffer);
		

	  c[i].tx = (float) x/w;
		x += g->bitmap.width+PADDING;

	  c[i].ax = g->advance.x >> 6;
	  c[i].ay = g->advance.y >> 6;

	  c[i].bw = g->bitmap.width;
	  c[i].bh = g->bitmap.rows;

	  c[i].bl = g->bitmap_left;
	  c[i].bt = g->bitmap_top;
	  /*
	  altough the struct contains only members of type float, you could use 
	  (u)int16_t or even (u)int8_t for them, depending on the maximum size
	   of the glyphs and the atlas. Since we are dealing with ASCII, we could 
	   also have omitted the copy of the g->advance.y value, since it should 
	   be 0 anyway. If you want to go beyond ASCII though, it is best not to 
	   make any assumptions.
	  */
	}

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	/* shader program */

	textshader = util_loadshaders("vertex_shader_text.glsl", "fragment_shader_text.glsl");
	glUseProgram(textshader);

	texUniformColour = glGetUniformLocation(textshader, "color");
	glUniform4f(texUniformColour, 0,0,0,1);
	glGenBuffers(1, &texBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, texBuffer);
	glBufferData(GL_ARRAY_BUFFER, MAX_TEXT_CHARS*6*4*4, NULL, GL_STATIC_DRAW);

	glGenVertexArrays(1, &texArrayObj);
	glBindVertexArray(texArrayObj);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	printf("max w,h = %d, %d\n", atlas_width, atlas_height);
	tex_n = 0;
}

static void render_text(const char *text, float x, float y, float sx, float sy) {

  const char *p;
  uint8_t i;
  int n = 0;

	struct point {
    GLfloat x;
    GLfloat y;
    GLfloat s;
    GLfloat t;
  } tex_coords[6 * strlen(text)];
  /* todo: bind glBindTexTexture? 
   */

  glUseProgram(textshader);
  // glActiveTexture(GL_TEXTURE0);
  // glBindTexture(GL_TEXTURE_2D, texID);
  // glUniform1i(texID, 0);
  // glBindVertexArray(texArrayObj);
  glBindBuffer(GL_ARRAY_BUFFER, texBuffer);


  for(p = text; *p; p++) {
  	i = (uint8_t) *p;
   	float x2 =  x + c[i].bl * sx;
    float y2 = -y - c[i].bt * sy;
    float w = c[i].bw * sx;
    float h = c[i].bh * sy;

    /* Advance the cursor to the start of the next character */
    x += c[i].ax * sx;
    y += c[i].ay * sy;

    /* Skip glyphs that have no pixels */
    if(!w || !h){
    	// printf("skipping pixel %c\n", *p);
      continue;
    }
    // printf("%f, %f, %f, %f, %f, %f, %f\n", x2, -y2, x2+w, -y2-h, c[i].tx, c[i].bl, c[i].bw);
    tex_coords[n++] = (struct point){x2,     -y2    , c[i].tx,  0};
    tex_coords[n++] = (struct point){x2 + w, -y2    , c[i].tx + c[i].bw / atlas_width, 0};
    tex_coords[n++] = (struct point){x2,     -y2 - h, c[i].tx, c[i].bh / atlas_height}; //remember: each glyph occupies a different amount of vertical space
    tex_coords[n++] = (struct point){x2 + w, -y2    , c[i].tx + c[i].bw / atlas_width, 0};
    tex_coords[n++] = (struct point){x2,     -y2 - h, c[i].tx, c[i].bh / atlas_height};
    tex_coords[n++] = (struct point){x2 + w, -y2 - h, c[i].tx + c[i].bw / atlas_width, c[i].bh / atlas_height};
  }
 	glBufferSubData(GL_ARRAY_BUFFER, tex_n*4*4, n*4*4, tex_coords); // counts are sizes in bytes
 	tex_n += n;
  // glDrawArrays(GL_TRIANGLES, 0, n);
}

static void draw_text(void)
{
	glUseProgram(textshader);
	glBindVertexArray(texArrayObj);
	// glBindBuffer(GL_ARRAY_BUFFER, texBuffer); /* don't need to bind, not changing it* */

	glDrawArrays(GL_TRIANGLES, 0, tex_n); // draw these points

	// tex_n = 0;
}


static void 
viewport_transform
(float x, float y, float width, float height, mat4f *T)
{
	float offset_x = (2*x + (width - window_width)) / window_width;
	float offset_y = (2*y + (height -window_height))/ window_height;

	float scale_x  = width / window_width;
	float scale_y  = height/ window_height;

	mat4f_copy(&mat4f_identity, T);
	mat4f_translate(&(const vec3f){offset_x, offset_y,0}, T);
	(*T)[0][0] = scale_x;
	(*T)[1][1] = scale_y;
}

int main()
{
	GLFWwindow* window = util_start("ex11-graph with textshader");
	assert(window != NULL);

	create_atlas();

	typedef struct {
	  GLfloat x;
	  GLfloat y;
	} point;

	point graph[20000];

	for(int i = 0; i < 20000; i++) {
	  float x = (i - 10000.0) / 1000.0;
	  graph[i].x = x;
	  graph[i].y = sinf(x * 10.0) / (1.0 + x * x); //expf(x); //cosf(x); //
	}

	static const point box[4] = {{-1, -1}, {1, -1}, {1 ,1}, {-1, 1} };

	point ticks[42];

	/* create vertex buffer object */

	// This will identify our vertex buffer
	/* | points  	vec2						| box  vec2 | ticks vec2 | */
	GLuint vbo;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(graph) + sizeof(box) + sizeof(ticks)*2,
		0, GL_STREAM_DRAW);

	glBufferSubData(
		GL_ARRAY_BUFFER,
		0,
		sizeof(graph),
		graph);
	glBufferSubData(
		GL_ARRAY_BUFFER,
		sizeof(graph),
		sizeof(box),
		box);

	

	/* create vertex array  */

	GLuint vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);

	/* shader program */

	GLuint shader = util_loadshaders("vertex_shader.glsl", "fragment_shader.glsl");
	glUseProgram(shader);

	GLuint u_transform = glGetUniformLocation(shader, "transform");
	GLuint u_colour 	 = glGetUniformLocation(shader, "colour");

	float scale = 1, offset = 0;
	GLenum mode = 0;
	mat4f T;

	/* for scissor*/
	
	/* begin main */

	glClearColor(1, 1, 1, 1);
	glLineWidth(4);

	// Cull triangles which normal is not towards the camera
	// glEnable(GL_CULL_FACE);

	while(!glfwWindowShouldClose(window) && 
	glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS) 
	{	
		/* set-up viewport for clipping and resizing */
		
		glfwGetWindowSize(window, &window_width, &window_height);
		glViewport(margin + ticksize, 
			margin + ticksize, 
			window_width - 2*margin - ticksize, 
			window_height-2*margin-ticksize);
#if 0		
		glScissor(margin + ticksize, 
			margin + ticksize, 
			width - 2*margin - ticksize, 
			height-2*margin-ticksize);

	glEnable(GL_SCISSOR_TEST);
#endif

		// wipe the drawing surface clear
		glClear(GL_COLOR_BUFFER_BIT);

		// play with inputs
		if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS ) { 
			scale *= 2;
		}
		if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) {
			scale *= 0.5;
		}
		if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
			offset += 0.1;
		}
		if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
			offset -= 0.1;
		}
		
		if (glfwGetKey(window, GLFW_KEY_F1) == GLFW_PRESS) {
			mode = GL_POINTS;
		}
		else if (glfwGetKey(window, GLFW_KEY_F2) == GLFW_PRESS) {
			mode = GL_LINE;
		}
		else if (glfwGetKey(window, GLFW_KEY_F3) == GLFW_PRESS) {
			mode = GL_LINE_STRIP;
		}
		else if (glfwGetKey(window, GLFW_KEY_F4) == GLFW_PRESS) {
			mode = GL_TRIANGLES;
		}
#if 1

		glUseProgram(shader);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBindVertexArray(vao);

		// draw these many points
		mat4f_copy(&mat4f_identity, &T);
		T[0][0] = scale;
		mat4f_translate( &(const vec3f){offset, 0, 0} , &T);
		glUniformMatrix4fv(u_transform, 1, GL_TRUE, &T[0][0]);
		glUniform4f(u_colour, 1, 0, 0, 1);
		glDrawArrays(mode, 0, 20000);

		/* draw box */
		
		glViewport(0, 0, window_width, window_height);
		viewport_transform(
			margin+ticksize, 
			margin+ticksize, 
			window_width - margin*2 - ticksize, 
			window_height - margin*2 - ticksize, 
			&T);
		glUniformMatrix4fv(u_transform, 1, GL_TRUE, &T[0][0]);

		glUniform4f(u_colour, 0,0,0,1); //todo: is this efficient? or send colours via buffer?
		glDrawArrays(GL_LINE_LOOP, 20000, 4);

		/* ticks */

		float pixel_x  = (float) 2 / (window_width  - margin*2 - ticksize);
		float pixel_y  = (float) 2 / (window_height - margin*2 - ticksize);
		for (int i=0; i <= 20; i++) {
			float y = -1 + i*0.1;
			ticks[i*2].x = -1;
			ticks[i*2].y = y;
			ticks[i*2+1].x = -1 - ticksize*pixel_x;
			ticks[i*2+1].y = y;
		}
		glBufferSubData(
			GL_ARRAY_BUFFER,
			sizeof(graph)+sizeof(box),
			sizeof(ticks),
			ticks); 
		glDrawArrays(GL_LINES, 20004, 42);

		float tickspacing = 0.1 * powf(10, -floor(log10(scale)));// desired space between ticks, in graph coordinates
		float left = -1.0/scale - offset;	// left edge, in graph coordinates
		float right = 1.0/scale - offset;// right edge, in graph coordinates
		int left_i = ceil(left / tickspacing);// index of left tick, counted from the origin
		int right_i = floor(right / tickspacing);// index of right tick, counted from the origin
		float rem = left_i * tickspacing - left;	// space between left edge of graph and the first tick
		int nticks = right_i - left_i + 1;	// number of ticks to show
		float firsttick = -1.0 + rem * scale;	// first tick in device coordinates
		if (nticks > 21)
			nticks = 21;	// should not happen

		for (int i = 0; i < nticks; i++) {
			float x = firsttick + i * tickspacing * scale;
			// float tickscale = ((i + left_i) % 10) ? 0.5 : 1;

			ticks[i * 2].x = x;
			ticks[i * 2].y = -1;
			ticks[i * 2 + 1].x = x;
			ticks[i * 2 + 1].y = -1 - ticksize * pixel_y;
		}
		glBufferSubData(
			GL_ARRAY_BUFFER,
			sizeof(graph)+sizeof(box)+sizeof(ticks),
			sizeof(float)*nticks*2*2,
			ticks); 
		glDrawArrays(GL_LINES, 20004+42, nticks*2);

#endif

		/* text */

		float sx = 2.0/ window_width;
		float sy = 2.0/ window_height;
		if (glfwGetTime() <= 5) {
			glViewport(0, 0, window_width, window_height);
			tex_n = 0;
			render_text("The,Quick Brown Fox Jumps Over The Lazy Dog",
		              -1 + 8 * sx,   1 - 50 * sy,    sx, sy);
			render_text("The Misaligned Fox Jumps Over The Lazy Dog",
	              -1 + 8.5 * sx, 1 - 100.5 * sy, sx, sy);
		  render_text("The Small Texture Scaled Fox Jumps Over The Lazy Dog",
	              -1 + 8 * sx,   1 - 175 * sy,   sx * 0.5, sy * 0.5);

		  render_text("The Small Font Sized Fox Jumps Over The Lazy Dog",
	              -1 + 8 * sx,   1 - 200 * sy,   sx, sy);

	  	render_text("The Tiny Texture Scaled Fox Jumps Over The Lazy Dog",
	              -1 + 8 * sx,   1 - 235 * sy,   sx * 0.25, sy * 0.25);

	  	printf("%d\n", tex_n);
		}
		draw_text();

		// update other events like input handling 
		glfwPollEvents();
		// put the stuff we've been drawing onto the display
		glfwSwapBuffers(window);
	}

	glDeleteBuffers(1, &vbo);
	glDeleteVertexArrays(1, &vao);
	glDeleteProgram(shader);
	glfwTerminate();
	return 0;
}