CC 			= gcc
AR 			= ar
CFLAGS 	= `pkg-config --cflags freetype2` -Wall -g -D USE_GLEW -Icommon/ -I../clib 
LFLAGS 	= `pkg-config --libs freetype2` \
					-lglfw -lGL -lGLEW -L. -lutil -lm \
          -L ~/GIT/clib -Wl,-rpath=../../clib -lmyclib

LIB   	= libutil.a

EX      = $(patsubst %.c,%.out,$(wildcard ex*/*.c))
LOBJS   = $(patsubst %.c,%.o,$(wildcard common/*.c))

default: $(EX) $(LIB)

%.a: $(LOBJS)
	$(AR) rcs $@ $^

%.out: %.o $(LIB)
	$(CC) -o $@ $< $(LFLAGS)

%.o: %.c
	$(CC) $(CFLAGS) -o $@ -c $<

clean:
	rm -f $(EX) $(LIB)
