#include "util.h"

// position
vec3f position = {0,0,5};

/* h.a. is in x-z world frame, 0 is +z axis
 * v.a. goes up towards +y axis 
 */

// horizontal angle : toward -Z
float horizontalAngle = 3.14;
// vertical angle : 0, look at the horizon
float verticalAngle = 0.0;
// Initial Field of View
float initialFoV = 45.0;

float speed = 3.0; // 3 units / second
float mouseSpeed = 0.001;

double t0 = 0;

void util_compute_camera_from_input(GLFWwindow *window, mat4f *view, mat4f *proj)
{
	double t = glfwGetTime();
	float deltaTime = t - t0;
	t0 = t;

	// Get mouse position	
	double xpos, ypos;
	glfwGetCursorPos(window, &xpos, &ypos);

	int width, height;
	glfwGetWindowSize(window, &width, &height);

	glfwSetCursorPos(window, width/2.0, height/2.0);

	// Compute new orientation
	horizontalAngle += mouseSpeed * (width/2.f - xpos );
	verticalAngle   += mouseSpeed * (height/2.f - ypos );

	// Direction : Spherical coordinates to Cartesian coordinates conversion
	vec3f direction = {
		cosf(verticalAngle) * sinf(horizontalAngle),
	 	sinf(verticalAngle),
		cosf(verticalAngle) * cosf(horizontalAngle)
  };

	// Right vector
	vec3f right = {
    sinf(horizontalAngle - 3.14f/2.0f),
    0,
    cosf(horizontalAngle - 3.14f/2.0f)
	};

	// Up vector : perpendicular to both direction and right
	vec3f up;
	vec3f_cross(&right, &direction, &up);

	// Move forward
	if (glfwGetKey(window, GLFW_KEY_UP ) == GLFW_PRESS){
		vec3f_add_s_mult(&position, &direction, deltaTime*speed, &position);
	}
	// Move backward
	if (glfwGetKey(window, GLFW_KEY_DOWN ) == GLFW_PRESS){
		vec3f_add_s_mult(&position, &direction, -deltaTime*speed, &position);
	}
	// Strafe right
	if (glfwGetKey(window, GLFW_KEY_RIGHT ) == GLFW_PRESS){
		vec3f_add_s_mult(&position, &right, deltaTime*speed, &position);
	}
	// Strafe left
	if (glfwGetKey(window, GLFW_KEY_LEFT ) == GLFW_PRESS){
		vec3f_add_s_mult(&position, &right, -deltaTime*speed, &position);
	}

	float FoV = radiansf(initialFoV); // - 5 * glfwMo

	mat4f_perspective(FoV, width/height, 0.1, 100.0, proj);
	
	mat4f_look_at(&position, vec3f_add(&direction, &position, &direction), 
		&up, view);
}


void util_compute_camera_rotate_around(GLFWwindow *window, mat4f *view, mat4f *proj)
{
	double t = glfwGetTime();
	float deltaTime = t - t0;
	t0 = t;

	// Get mouse position	
	double xpos, ypos;
	glfwGetCursorPos(window, &xpos, &ypos);

	int width, height;
	glfwGetWindowSize(window, &width, &height);

	glfwSetCursorPos(window, width/2.0, height/2.0);
	printf("%.3f, %.3f\n", xpos, ypos);
	// Compute new orientation
	horizontalAngle += mouseSpeed * (width/2.f - xpos );
	verticalAngle   += mouseSpeed * (height/2.f - ypos );

	// Direction : Spherical coordinates to Cartesian coordinates conversion
	vec3f direction = {
		cosf(verticalAngle) * sinf(horizontalAngle),
	 	sinf(verticalAngle),
		cosf(verticalAngle) * cosf(horizontalAngle)
  };

	// Right vector
	vec3f right = {
    sinf(horizontalAngle - 3.14f/2.0f),
    0,
    cosf(horizontalAngle - 3.14f/2.0f)
	};

	// Up vector : perpendicular to both direction and right
	vec3f up;
	vec3f_cross(&right, &direction, &up);

	// Move forward
	if (glfwGetKey(window, GLFW_KEY_UP ) == GLFW_PRESS){
		vec3f_add_s_mult(&position, &direction, deltaTime*speed, &position);
	}
	// Move backward
	if (glfwGetKey(window, GLFW_KEY_DOWN ) == GLFW_PRESS){
		vec3f_add_s_mult(&position, &direction, -deltaTime*speed, &position);
	}
	// Strafe right
	if (glfwGetKey(window, GLFW_KEY_RIGHT ) == GLFW_PRESS){
		vec3f_add_s_mult(&position, &right, deltaTime*speed, &position);
	}
	// Strafe left
	if (glfwGetKey(window, GLFW_KEY_LEFT ) == GLFW_PRESS){
		vec3f_add_s_mult(&position, &right, -deltaTime*speed, &position);
	}

	float FoV = radiansf(initialFoV); // - 5 * glfwMo

	mat4f_perspective(FoV, width/height, 0.1, 100.0, proj);
	
	mat4f_look_at(&position, vec3f_add(&direction, &position, &direction), 
		&up, view);
}

void util_compute_rotate_camera_from_input(GLFWwindow *window, mat4f *view, mat4f *proj)
{
	static float R = 10, omega1 = 0;
	static vec3f tar = {0};
	double t = glfwGetTime();
	float deltaTime = t - t0;
	t0 = t;


	// Get mouse position	
	double xpos, ypos;
	glfwGetCursorPos(window, &xpos, &ypos);

	int width, height;
	glfwGetWindowSize(window, &width, &height);

	// glfwSetCursorPos(window, width/2.0, height/2.0);

	// omega1 = 	 -(xpos - width/2.f) / (width/2.f) * 3.14;
	
	// Move forward
	if (glfwGetKey(window, GLFW_KEY_UP ) == GLFW_PRESS){
		R += deltaTime*speed;
	}
	// Move backward
	if (glfwGetKey(window, GLFW_KEY_DOWN ) == GLFW_PRESS){
		R -= deltaTime*speed;
	}
	// Strafe right
	if (glfwGetKey(window, GLFW_KEY_RIGHT ) == GLFW_PRESS){
		omega1 += deltaTime;
	}
	// Strafe left
	if (glfwGetKey(window, GLFW_KEY_LEFT ) == GLFW_PRESS){
		omega1 -= deltaTime;
	}

	horizontalAngle = omega1*t;
	verticalAngle   =  (ypos - height/2.f)/ (height/2.f) * 3.14;

	/* spherical to cartesian */

	position[0] = R * cosf(verticalAngle) * sinf(horizontalAngle);
	position[1] = R * sinf(verticalAngle);
	position[2] = R * cosf(verticalAngle) * cosf(horizontalAngle);


	// Right vector
	vec3f right = {
    sinf(horizontalAngle - 3.14f/2.0f),
    0,
    cosf(horizontalAngle - 3.14f/2.0f)
	};

	// Up vector : perpendicular to both direction and right
	vec3f up;
	vec3f_cross(&right, &position, &up);


	float FoV = radiansf(initialFoV); // - 5 * glfwMo

	mat4f_perspective(FoV, width/height, 0.1, 100.0, proj);
	mat4f_look_at(&position, &tar, &up, view);
}