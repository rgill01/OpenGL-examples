#include "util.h"
#include <stdio.h>

GLFWwindow* util_start(const char *title)
{
  // start GL context and O/S window using the GLFW helper library
  if (!glfwInit()) {
    fprintf(stderr, "ERROR: could not start GLFW3\n");
    return NULL;
  } 

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  GLFWwindow* window = glfwCreateWindow(800, 600, title, NULL, NULL);
  if (!window) {
    fprintf(stderr, "ERROR: could not open window with GLFW3\n");
    glfwTerminate();
    return NULL;
  }
  glfwMakeContextCurrent(window);
                                  
  // start GLEW extension handler
#ifdef USE_GLEW
  glewExperimental = GL_TRUE;
  if (glewInit())
  {
      fprintf(stderr, "Failed to initialize GLEW.\n");
  }
#endif

  // get version info
  const GLubyte* renderer = glGetString(GL_RENDERER); // get renderer string
  const GLubyte* version = glGetString(GL_VERSION); // version as a string
  printf("Renderer: %s\n", renderer);
  printf("OpenGL version supported %s\n", version);



  // tell GL to only draw onto a pixel if the shape is closer to the viewer
  // glEnable(GL_DEPTH_TEST); // enable depth-testing
  // glDepthFunc(GL_LESS); // depth-testing interprets a smaller value as "closer

  return window;
}