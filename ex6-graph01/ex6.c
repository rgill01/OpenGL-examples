#include <util.h>
#include <stdio.h>
#include <assert.h>
#include <vectorf.h>
#include <utilities.h>


int main()
{
	GLFWwindow* window = util_start("ex6-graph01");
	assert(window != NULL);

	typedef struct {
	  GLfloat x;
	  GLfloat y;
	} point;

	point graph[2000];

	for(int i = 0; i < 2000; i++) {
	  float x = (i - 1000.0) / 100.0;
	  graph[i].x = x;
	  graph[i].y = sinf(x * 10.0) / (1.0 + x * x);
	}

	/* create vertex buffer object */

	// This will identify our vertex buffer
	GLuint vbo;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(graph), 
		graph, GL_STATIC_DRAW);

	/* create vertex array  */

	GLuint vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);

	/* shader program */

	GLuint shader = util_loadshaders("vertex_shader.glsl", "fragment_shader.glsl");
	glUseProgram(shader);

	GLuint scaleID = glGetUniformLocation(shader, "scale");
	GLuint offsetID = glGetUniformLocation(shader, "offset");

	// glUniform1f(scaleID,  0.1);
	// glUniform1f(offsetID, 0);
	float scale = 1, offset = 0;
	GLenum mode = 0;
	/* begin main */

	glClearColor(0.3, 0.3, 0.3, 1.0);

	glLineWidth(5);
	// Cull triangles which normal is not towards the camera
	// glEnable(GL_CULL_FACE);

	while(!glfwWindowShouldClose(window) && 
	glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS) 
	{

		// wipe the drawing surface clear
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);



		// play with inputs

		if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS ) { 
			scale *= 2;
		}
		if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) {
			scale *= 0.5;
		}
		if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
			offset += 0.1;
		}
		if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
			offset -= 0.1;
		}
		
		if (glfwGetKey(window, GLFW_KEY_F1) == GLFW_PRESS) {
			mode = GL_POINTS;
		}
		else if (glfwGetKey(window, GLFW_KEY_F2) == GLFW_PRESS) {
			mode = GL_LINE;
		}
		else if (glfwGetKey(window, GLFW_KEY_F3) == GLFW_PRESS) {
			mode = GL_LINE_STRIP;
		}
		else if (glfwGetKey(window, GLFW_KEY_F4) == GLFW_PRESS) {
			mode = GL_TRIANGLES;
		}

		glUniform1f(scaleID, scale);
		glUniform1f(offsetID, offset);




		// draw these many points
		glDrawArrays(mode, 0, 2000);

		// update other events like input handling 
		glfwPollEvents();
		// put the stuff we've been drawing onto the display
		glfwSwapBuffers(window);
	}

	glDeleteBuffers(1, &vbo);
	glDeleteVertexArrays(1, &vao);
	glDeleteProgram(shader);
	glfwTerminate();
	return 0;
}