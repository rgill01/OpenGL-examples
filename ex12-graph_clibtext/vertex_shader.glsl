#version 400
layout(location = 0) in vec2 pt;
uniform mat4 transform;

void main(void)
{
	gl_Position = transform * vec4( pt.xy, 0, 1);
}