#include <util.h>
#include <stdio.h>
#include <assert.h>
#include <vectorf.h>
#include <utilities.h>

#include <freetype2/ft2build.h>
#include FT_FREETYPE_H

#define FONT_TFF 			"/usr/local/texlive/2016/texmf-dist/fonts/truetype/public/comfortaa/Comfortaa-Light.ttf"
#define PADDING 			0

static int window_width, window_height;

static GLuint tex;
static int atlas_width, atlas_height;
static struct character_info {
  float ax; // advance.x
  float ay; // advance.y
  
  float bw; // bitmap.width;
  float bh; // bitmap.rows;
  
  float bl; // bitmap_left;
  float bt; // bitmap_top;
  
  float tx; // x offset of glyph in texture coordinates
} c[128] = {{0}};

static void create_atlas(const FT_Face face)
{
	FT_GlyphSlot g = face->glyph;
	unsigned int w = 0;
	unsigned int h = 0;

	for(int i = 32; i < 128; i++) {
	  if(FT_Load_Char(face, i, FT_LOAD_RENDER)) {
	    fprintf(stderr, "Loading character %d:%c failed!\n", i,i);
	    continue;
  	}
  	w += g->bitmap.width + PADDING;
  	h = maxui(h, g->bitmap.rows);
	}

	/* you might as well save this value as it is needed later on */
	atlas_width = w;
	atlas_height = h;

	glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);


	glTexImage2D(
		GL_TEXTURE_2D, 
		0, 
		GL_RED, 
		w, 
		h, 
		0, 
		GL_RED, 
		GL_UNSIGNED_BYTE, 
		0);


	int x = 0;
	for(int i = 32; i < 128; i++) {
  	if(FT_Load_Char(face, i, FT_LOAD_RENDER)){
  		printf("error loading char %d\n", i);
    	continue;
  	}

  	glTexSubImage2D(
  		GL_TEXTURE_2D, 
  		0, 
  		x, 
  		0, 
  		g->bitmap.width+PADDING, 
  		g->bitmap.rows, 
  		GL_RED, 
  		GL_UNSIGNED_BYTE, 
  		g->bitmap.buffer);
		

	  c[i].tx = (float) x/w;
		x += g->bitmap.width+PADDING;

	  c[i].ax = g->advance.x >> 6;
	  c[i].ay = g->advance.y >> 6;

	  c[i].bw = g->bitmap.width;
	  c[i].bh = g->bitmap.rows;

	  c[i].bl = g->bitmap_left;
	  c[i].bt = g->bitmap_top;
	  /*
	  altough the struct contains only members of type float, you could use 
	  (u)int16_t or even (u)int8_t for them, depending on the maximum size
	   of the glyphs and the atlas. Since we are dealing with ASCII, we could 
	   also have omitted the copy of the g->advance.y value, since it should 
	   be 0 anyway. If you want to go beyond ASCII though, it is best not to 
	   make any assumptions.
	  */
	}

	printf("max w,h = %d, %d\n", atlas_width, atlas_height);
}

static void render_text(const char *text, float x, float y, float sx, float sy) {
  const char *p;
  struct point {
    GLfloat x;
    GLfloat y;
    GLfloat s;
    GLfloat t;
  } coords[6 * strlen(text)];
  int n = 0;
  uint8_t i;

  for(p = text; *p; p++) {
  	i = (uint8_t) *p;
   	float x2 =  x + c[i].bl * sx;
    float y2 = -y - c[i].bt * sy;
    float w = c[i].bw * sx;
    float h = c[i].bh * sy;

    /* Advance the cursor to the start of the next character */
    x += c[i].ax * sx;
    y += c[i].ay * sy;

    /* Skip glyphs that have no pixels */
    if(!w || !h){
    	// printf("skipping pixel %c\n", *p);
      continue;
    }
    // printf("%f, %f, %f, %f, %f, %f, %f\n", x2, -y2, x2+w, -y2-h, c[i].tx, c[i].bl, c[i].bw);
    coords[n++] = (struct point){x2,     -y2    , c[i].tx,  0};
    coords[n++] = (struct point){x2 + w, -y2    , c[i].tx + c[i].bw / atlas_width, 0};
    coords[n++] = (struct point){x2,     -y2 - h, c[i].tx, c[i].bh / atlas_height}; //remember: each glyph occupies a different amount of vertical space
    coords[n++] = (struct point){x2 + w, -y2    , c[i].tx + c[i].bw / atlas_width, 0};
    coords[n++] = (struct point){x2,     -y2 - h, c[i].tx, c[i].bh / atlas_height};
    coords[n++] = (struct point){x2 + w, -y2 - h, c[i].tx + c[i].bw / atlas_width, c[i].bh / atlas_height};
  }

  glBufferData(GL_ARRAY_BUFFER, sizeof(coords), coords, GL_DYNAMIC_DRAW);
  glDrawArrays(GL_TRIANGLES, 0, n);
}




int main(void)
{
	GLFWwindow* window = util_start("ex10-textatlas");
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	assert(window != NULL);

	FT_Library ft;
	if(FT_Init_FreeType(&ft)) {
		fprintf(stderr, "Could not init freetype library\n");
		return 1;
	}

	FT_Face face;
	if(FT_New_Face(ft, FONT_TFF, 0, &face)) {
	  fprintf(stderr, "Could not open font\n");
	  return 1;
	} 
	else if (face == NULL){
		printf("face is NULL\n");
		return 1;
	}
	FT_Set_Pixel_Sizes(face, 0, 48);	
	create_atlas(face);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	/* shader program */

	GLuint shader = util_loadshaders("vertex_shader.glsl", "fragment_shader.glsl");
	glUseProgram(shader);

	/* vbo */

	GLuint vbo;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	/* vao */

	GLuint vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	/* uniforms */

	GLuint uniform_color = glGetUniformLocation(shader, "color");
	
	// GLuint uniform_tex   = glGetUniformLocation(shader, "tex");
	// glUniform1i(uniform_tex, 0);
	// glBindTexture(GL_TEXTURE_2D, tex);

	/* main loop */

	glClearColor(1, 1, 1, 1);

	while(!glfwWindowShouldClose(window) && 
	glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS) 
	{	
		glClear(GL_COLOR_BUFFER_BIT);		
		glViewport(0, 0, window_width, window_height);


	  GLfloat black[4] = {0, 0, 0, 1};
	  glUniform4fv(uniform_color, 1, black);

	  glfwGetWindowSize(window, &window_width, &window_height);
	  float sx = 2.0 / window_width;
	  float sy = 2.0 / window_height; /* to convert pixels to [-1,1] */

	  render_text("The Quick Brown Fox Jumps Over The Lazy Dog",
	              -1 + 8 * sx,   1 - 50 * sy,    sx, sy);
	  render_text("The Misaligned Fox Jumps Over The Lazy Dog",
	              -1 + 8.5 * sx, 1 - 100.5 * sy, sx, sy);
#if 1
    FT_Set_Pixel_Sizes(face, 0, 48);
	  render_text("The Small Texture Scaled Fox Jumps Over The Lazy Dog",
	              -1 + 8 * sx,   1 - 175 * sy,   sx * 0.5, sy * 0.5);

	  FT_Set_Pixel_Sizes(face, 0, 24);
	  render_text("The Small Font Sized Fox Jumps Over The Lazy Dog",
	              -1 + 8 * sx,   1 - 200 * sy,   sx, sy);

	  FT_Set_Pixel_Sizes(face, 0, 48);
	  render_text("The Tiny Texture Scaled Fox Jumps Over The Lazy Dog",
	              -1 + 8 * sx,   1 - 235 * sy,   sx * 0.25, sy * 0.25);

	  FT_Set_Pixel_Sizes(face, 0, 12);
	  render_text("The Tiny Font Sized Fox Jumps Over The Lazy Dog",
	              -1 + 8 * sx,   1 - 250 * sy,   sx, sy);


	  FT_Set_Pixel_Sizes(face, 0, 48);
  	render_text("The Solid Black Fox Jumps Over The Lazy Dog",
              -1 + 8 * sx,   1 - 430 * sy,   sx, sy);

  	GLfloat red[4] = {1, 0, 0, 1};
  	glUniform4fv(uniform_color, 1, red);
	  render_text("The Solid Red Fox Jumps Over The Lazy Dog",
	              -1 + 8 * sx,   1 - 330 * sy,   sx, sy);
	  render_text("The Solid Red Fox Jumps Over The Lazy Dog",
	              -1 + 28 * sx,  1 - 450 * sy,   sx, sy);

	  GLfloat transparent_green[4] = {0, 1, 0, 0.5};
	  glUniform4fv(uniform_color, 1, transparent_green);
	  render_text("The Transparent Green Fox Jumps Over The Lazy Dog",
	              -1 + 8 * sx,   1 - 380 * sy,   sx, sy);
	  render_text("The Transparent Green Fox Jumps Over The Lazy Dog",
	              -1 + 18 * sx,  1 - 440 * sy,   sx, sy);

#endif
		// update other events like input handling 
		glfwPollEvents();
		// put the stuff we've been drawing onto the display
		glfwSwapBuffers(window);
	}

	glDeleteBuffers(1, &vbo);
	glDeleteVertexArrays(1, &vao);
	glDeleteProgram(shader);
	glfwTerminate();
	return 0;
}