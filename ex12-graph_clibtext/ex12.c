#include <util.h>
#include <stdio.h>
#include <assert.h>
#include <vectorf.h>
#include <utilities.h>
#include <glutilities.h>


static const int margin 	= 20;
static const int ticksize = 10;
int window_width, window_height;



static void 
viewport_transform
(float x, float y, float width, float height, mat4f *T)
{
	float offset_x = (2*x + (width - window_width)) / window_width;
	float offset_y = (2*y + (height -window_height))/ window_height;

	float scale_x  = width / window_width;
	float scale_y  = height/ window_height;

	mat4f_copy(&mat4f_identity, T);
	mat4f_translate(&(const vec3f){offset_x, offset_y,0}, T);
	(*T)[0][0] = scale_x;
	(*T)[1][1] = scale_y;
}

int main()
{
	GLFWwindow* window = util_start("ex11-graph with textshader");
	assert(window != NULL);

	glutil_text * text = glutil_text_initiate(12);

	typedef struct {
	  GLfloat x;
	  GLfloat y;
	} point;

	point graph[20000];

	for(int i = 0; i < 20000; i++) {
	  float x = (i - 10000.0) / 1000.0;
	  graph[i].x = x;
	  graph[i].y = sinf(x * 10.0) / (1.0 + x * x); //expf(x); //cosf(x); //
	}

	static const point box[4] = {{-1, -1}, {1, -1}, {1 ,1}, {-1, 1} };

	point ticks[42];

	/* create vertex buffer object */

	// This will identify our vertex buffer
	/* | points  	vec2						| box  vec2 | ticks vec2 | */
	GLuint vbo;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(graph) + sizeof(box) + sizeof(ticks)*2,
		0, GL_STREAM_DRAW);

	glBufferSubData(
		GL_ARRAY_BUFFER,
		0,
		sizeof(graph),
		graph);
	glBufferSubData(
		GL_ARRAY_BUFFER,
		sizeof(graph),
		sizeof(box),
		box);

	

	/* create vertex array  */

	GLuint vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);

	/* shader program */

	GLuint shader = util_loadshaders("vertex_shader.glsl", "fragment_shader.glsl");
	glUseProgram(shader);

	GLuint u_transform = glGetUniformLocation(shader, "transform");
	GLuint u_colour 	 = glGetUniformLocation(shader, "colour");

	float scale = 1, offset = 0;
	GLenum mode = 0;
	mat4f T;

	/* for scissor*/
	
	/* begin main */

	glClearColor(1, 1, 1, 1);
	glLineWidth(4);

	// Cull triangles which normal is not towards the camera
	// glEnable(GL_CULL_FACE);

	while(!glfwWindowShouldClose(window) && 
	glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS) 
	{	
		/* set-up viewport for clipping and resizing */
		
		glfwGetWindowSize(window, &window_width, &window_height);
		glViewport(margin + ticksize, 
			margin + ticksize, 
			window_width - 2*margin - ticksize, 
			window_height-2*margin-ticksize);
#if 0		
		glScissor(margin + ticksize, 
			margin + ticksize, 
			width - 2*margin - ticksize, 
			height-2*margin-ticksize);

	glEnable(GL_SCISSOR_TEST);
#endif

		// wipe the drawing surface clear
		glClear(GL_COLOR_BUFFER_BIT);

		// play with inputs
		if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS ) { 
			scale *= 2;
		}
		if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) {
			scale *= 0.5;
		}
		if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
			offset += 0.1;
		}
		if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
			offset -= 0.1;
		}
		
		if (glfwGetKey(window, GLFW_KEY_F1) == GLFW_PRESS) {
			mode = GL_POINTS;
		}
		else if (glfwGetKey(window, GLFW_KEY_F2) == GLFW_PRESS) {
			mode = GL_LINE;
		}
		else if (glfwGetKey(window, GLFW_KEY_F3) == GLFW_PRESS) {
			mode = GL_LINE_STRIP;
		}
		else if (glfwGetKey(window, GLFW_KEY_F4) == GLFW_PRESS) {
			mode = GL_TRIANGLES;
		}
#if 1

		glUseProgram(shader);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBindVertexArray(vao);

		// draw these many points
		mat4f_copy(&mat4f_identity, &T);
		T[0][0] = scale;
		mat4f_translate( &(const vec3f){offset, 0, 0} , &T);
		glUniformMatrix4fv(u_transform, 1, GL_TRUE, &T[0][0]);
		glUniform4f(u_colour, 1, 0, 0, 1);
		glDrawArrays(mode, 0, 20000);

		/* draw box */
		
		glViewport(0, 0, window_width, window_height);
		viewport_transform(
			margin+ticksize, 
			margin+ticksize, 
			window_width - margin*2 - ticksize, 
			window_height - margin*2 - ticksize, 
			&T);
		glUniformMatrix4fv(u_transform, 1, GL_TRUE, &T[0][0]);

		glUniform4f(u_colour, 0,0,0,1); //todo: is this efficient? or send colours via buffer?
		glDrawArrays(GL_LINE_LOOP, 20000, 4);

		/* ticks */

		float pixel_x  = (float) 2 / (window_width  - margin*2 - ticksize);
		float pixel_y  = (float) 2 / (window_height - margin*2 - ticksize);
		for (int i=0; i <= 20; i++) {
			float y = -1 + i*0.1;
			ticks[i*2].x = -1;
			ticks[i*2].y = y;
			ticks[i*2+1].x = -1 - ticksize*pixel_x;
			ticks[i*2+1].y = y;
		}
		glBufferSubData(
			GL_ARRAY_BUFFER,
			sizeof(graph)+sizeof(box),
			sizeof(ticks),
			ticks); 
		glDrawArrays(GL_LINES, 20004, 42);

		float tickspacing = 0.1 * powf(10, -floor(log10(scale)));// desired space between ticks, in graph coordinates
		float left = -1.0/scale - offset;	// left edge, in graph coordinates
		float right = 1.0/scale - offset;// right edge, in graph coordinates
		int left_i = ceil(left / tickspacing);// index of left tick, counted from the origin
		int right_i = floor(right / tickspacing);// index of right tick, counted from the origin
		float rem = left_i * tickspacing - left;	// space between left edge of graph and the first tick
		int nticks = right_i - left_i + 1;	// number of ticks to show
		float firsttick = -1.0 + rem * scale;	// first tick in device coordinates
		if (nticks > 21)
			nticks = 21;	// should not happen

		for (int i = 0; i < nticks; i++) {
			float x = firsttick + i * tickspacing * scale;
			// float tickscale = ((i + left_i) % 10) ? 0.5 : 1;

			ticks[i * 2].x = x;
			ticks[i * 2].y = -1;
			ticks[i * 2 + 1].x = x;
			ticks[i * 2 + 1].y = -1 - ticksize * pixel_y;
		}
		glBufferSubData(
			GL_ARRAY_BUFFER,
			sizeof(graph)+sizeof(box)+sizeof(ticks),
			sizeof(float)*nticks*2*2,
			ticks); 
		glDrawArrays(GL_LINES, 20004+42, nticks*2);

#endif

		/* text */

		if (glfwGetTime() <= 5) {
			// glViewport(0, 0, window_width, window_height);
			glutil_text_render(text, 
				(const glutil_text_data[]){ 
					{"The,Quick Brown Fox Jumps Over The Lazy Dog 0123456789", 1, 10, 255,0,0}, 
					{"The,Quick Brown Fox Jumps Over The Lazy Dog 0123456789", 1, 300, 0,255,0},
					{"The,Quick Brown Fox Jumps Over The Lazy Dog 0123456789", 1, 590, 0,0,255},},
					3, window_width, window_height);
		}
		glutil_text_draw(text);

		// update other events like input handling 
		glfwPollEvents();
		// put the stuff we've been drawing onto the display
		glfwSwapBuffers(window);
	}

	glDeleteBuffers(1, &vbo);
	glDeleteVertexArrays(1, &vao);
	glDeleteProgram(shader);
	glfwTerminate();
	glutil_text_destroy(text);
	return 0;
}