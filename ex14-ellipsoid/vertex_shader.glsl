#version 400
layout(location = 0) in vec3 pos; // in model space
layout(location = 1) in vec3 colour;
layout(location = 2) in vec3 normal; // in model space

out vec3 frag_colour;

// Values that stay constant for the whole mesh.
uniform mat4 MVP;  // camera to model with perspective distortion
uniform mat3 Q; //  defines the ellipse as the range space of Q subject to unit ball
uniform float lightPower; 
uniform vec3 lightSource; // in model frame

void main()
{
	gl_Position = MVP * vec4(Q*pos, 1.0);

	vec3 lightRay = lightSource - pos; /* could compute all this in FS */
	vec3 l = normalize(lightRay);
	vec3 n = normalize(normal);
	float cosTheta = max( dot(n,l), 0 );
	float dist = length(lightRay);

	float ambient = 0.1;
	float diffuse =(lightPower * cosTheta / (dist * dist));

	//frag_colour = (ambient + diffuse) * colour;
	frag_colour = colour;
}