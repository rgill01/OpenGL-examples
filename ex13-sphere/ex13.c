#include <util.h>
#include <stdio.h>
#include <assert.h>
#include <vectorf.h>
#include <utilities.h>
#include <math.h>

typedef struct 
{
	GLfloat x;
	GLfloat y;
	GLfloat z;
} point;

#define SLICES 50
#define INTERVAL (2*M_PI/SLICES)
#define NO_PTS (SLICES*SLICES)*6

static point g_points[NO_PTS];
static point g_normals[NO_PTS];

point F(float r, float th, float phi)
{
	point p;

	p.x = r*cosf(phi)*cosf(th);
	p.y = r*cosf(phi)*sinf(th);
	p.z = r*sinf(phi);

	return p;
}

int generate_sphere_points(GLfloat r) 
{
	float th, phi;
	int i = 0;
	for (th = 0; th <= 2*M_PI; th += M_PI_4){
		for (phi = -M_PI; phi <= M_PI; phi += INTERVAL) {
			g_points[i] = F(r, th, phi);
			i++;
		}
	}
	return i;
}

int generate_sphere_points2(GLfloat r) 
{
	float th, phi;
	int i = 0;
	for (th = 0; th <= 2*M_PI; th += INTERVAL){
		for (phi = -M_PI; phi <= M_PI; phi += INTERVAL) {
			g_normals[i]  = F(1, th, phi);
			g_points[i++] = F(r, th, phi);

			g_normals[i]  = F(1, th + INTERVAL, phi);
			g_points[i++] = F(r, th + INTERVAL, phi);

			g_normals[i]  = F(1, th, phi + INTERVAL);
			g_points[i++] = F(r, th, phi + INTERVAL);

			g_normals[i] 	= F(1, th + INTERVAL, phi + INTERVAL);
			g_points[i++] = F(r, th + INTERVAL, phi + INTERVAL);
			
			g_normals[i] 	= F(1, th + INTERVAL, phi);
			g_points[i++] = F(r, th + INTERVAL, phi);
			
			g_normals[i] 	= F(1, th, phi + INTERVAL);
			g_points[i++] = F(r, th, phi + INTERVAL);
		}
	}
	return i;
}

point g_colours[NO_PTS] = {[0 ... NO_PTS-1] = {1,1,0}};

int main()
{
	GLFWwindow* window = util_start("ex3-triangle coloured");
	assert(window != NULL);

	int no_pts = generate_sphere_points2(0.5);
	/* create vertex buffer object */

	// This will identify our vertex buffer
	GLuint vertexbuffer;
	// Generate 1 buffer, put the resulting identifier in vertexbuffer
	glGenBuffers(1, &vertexbuffer);
	// The following commands will talk about our 'vertexbuffer' buffer
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	// Give our vertices to OpenGL.


	glBufferData(GL_ARRAY_BUFFER, 
	sizeof(g_points), g_points, GL_STATIC_DRAW);

	/* buffer: [ CUBE    | Triangle ] 
	*                   ^ sizeof(g_vertex_buffer_data)
	* https://youtu.be/xOqlz9bot-Y
	*/

	// glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(g_vertex_buffer_data), g_vertex_buffer_data);
	// glBufferSubData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), sizeof(triangle_vertex), triangle_vertex);

	/* and one for colours */
	GLuint colourBuffer;
	glGenBuffers(1, &colourBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, colourBuffer);
	glBufferData(GL_ARRAY_BUFFER, 
	sizeof(g_colours), g_colours, GL_STATIC_DRAW);
	// glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(g_color_buffer_), g_color_buffer_data);
	// glBufferSubData(GL_ARRAY_BUFFER, sizeof(g_color_buffer_data), sizeof(triangle_colours), triangle_colours);

	/* one for normals */
	GLuint normalBuffer;
	glGenBuffers(1, &normalBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_normals), g_normals, GL_STATIC_DRAW);

	/* create vertex array  */

	GLuint vertexArrayObject;
	glGenVertexArrays(1, &vertexArrayObject);
	glBindVertexArray(vertexArrayObject);

	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glBindBuffer(GL_ARRAY_BUFFER, colourBuffer);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);

	/* shader program */

	GLuint shader = util_loadshaders("vertex_shader.glsl", "fragment_shader.glsl");

	mat4f T;
	GLuint mvpID = glGetUniformLocation(shader, "MVP");
	GLuint mvID  = glGetUniformLocation(shader, "MV");
	GLuint lightPowerID = glGetUniformLocation(shader, "lightPower");
	GLuint lightSourceID = glGetUniformLocation(shader, "lightSource");
	glUseProgram(shader);

	glClearColor(0.3, 0.3, 0.3, 1.0);


	// Cull triangles which normal is not towards the camera
	// glEnable(GL_CULL_FACE);
	// Enable blending	
	glEnable(GL_BLEND);
	// glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	// tell GL to only draw onto a pixel if the shape is closer to the viewer
  glEnable(GL_DEPTH_TEST); // enable depth-testing
  // glDepthFunc(GL_LESS); // depth-testing interprets a smaller value as "closer

	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	glUniform1f(lightPowerID, 60);
	glUniform3f(lightSourceID, 0, 5, 5);

	while(!glfwWindowShouldClose(window) && 
	glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS) 
	{
		// wipe the drawing surface clear
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Compute the MVP matrix from keyboard and mouse input
		mat4f proj, view;
    util_compute_camera_from_input(window, &view, &proj);
    // util_compute_rotate_camera_from_input(window, &view, &proj);

    /* transform */
		// rotf q; 
		// rotf_from_yaw(radiansf(0), &q);
		mat4f_rot_trans_camera(&rotf_identity, &vec3f_zero, &view, &proj, &T);
		glUniformMatrix4fv(mvpID, 1, GL_TRUE, (GLfloat *) &T[0][0]);
		glUniformMatrix4fv(mvID, 1, GL_TRUE, (GLfloat *) &view[0][0]);


		// Note: this call is not necessary, but I like to do it anyway before any
		// time that I call glDrawArrays() so I never use the wrong shader programme
		// glUseProgram( shader );

		// Note: this call is not necessary, but I like to do it anyway before any
		// time that I call glDrawArrays() so I never use the wrong vertex data
		// glBindVertexArray(vertexArrayObject);

		// draw these many points
		glDrawArrays(GL_TRIANGLES, 0, no_pts);

		// update other events like input handling 
		glfwPollEvents();
		// put the stuff we've been drawing onto the display
		glfwSwapBuffers(window);
	}





	glDeleteBuffers(1, &vertexbuffer);
	glDeleteVertexArrays(1, &vertexArrayObject);
	glDeleteProgram(shader);
	glfwTerminate();
	return 0;
}